App.LeadController = Ember.ObjectController.extend({
  actions: {
    saveChanges: function() {
      this.get('model').save();
    }
  },
  
  hasUnsavedStuff: function(){
    return this.get('isDirty') && !this.get('isSaving');
  }.property('isDirty', 'isSaving')
  
});