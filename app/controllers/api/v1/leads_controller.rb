class Api::V1::LeadsController < ApplicationController
  respond_to :json
  
  def index
    respond_with leads: Lead.all #pass hash so ember caches it
  end
  
  def show
    respond_with lead
  end
  
  def create
    respond_with :api, :v1, Lead.create(lead_params)
  end
  
  def update
    lead.update(lead_params) 
    respond_with lead # so it returns 204 or 422
  end
  
  def destroy
    respond_with lead.destroy
  end
  
  private
  
    def lead
      @lead ||= Lead.find(params[:id]) #don't hit the database everytime
    end
    
    def lead_params
      params.require(:lead).permit(:first_name, :last_name, :email, :phone,
            :status, :notes)
    end
    
end